# Testbed

## Prerequisites

Make sure you have **Docker** [1] and **Docker Compose** [2] available on your
local environment.

## Run

Execute the following command to run the backend.

    docker-compose up

The base URL for the backend is **http://localhost:3001**.

Import **quotes-backend.postman-collection.json** collection in Postman. Use it
to verify that the backend is running (e.g. by getting all quotes).

## References

[1] https://docs.docker.com

[2] https://docs.docker.com/compose
