import React from "react";
import { NavLink, useNavigate } from "react-router-dom";

import QuotesList from "./QuotesList";
import Layout from "components/Layout";

export default function Quotes(): JSX.Element {
  const navigate = useNavigate();

  return (
    <Layout>
      <div className="2xl:max-h-2/3 mx-auto overflow-hidden border-gray-200 flex flex-col w-full max-w-4xl">
        <div className="mb-8 mx-auto text-center">
          <h1 className="text-5xl">Inspiring quotes*</h1>
          <p className="text-gray-400 text-left">*If you need them</p>

          <button
            className="rounded-md border border-gray-300 py-2 px-4 mt-3 min-w-full max-w-xs bg-gray-200"
            onClick={() => navigate("/quote-daily")}
          >
            <h2 className="text-2xl text-yellow-600">Quote of the Day</h2>
          </button>
        </div>

        <div className="2xl:max-h-2/3 mx-auto overflow-hidden border border-gray-200 flex flex-row w-full max-w-4xl shadow-sm">
          <div className="w-full bg-white">
            <QuotesList />
          </div>
        </div>

        <NavLink to={`/new-quote`}>
          <button className="bg-blue-550 border text-white rounded-md py-2 px-4 font-bold mt-3 max-w-xs">
            Add a quote
          </button>
        </NavLink>
      </div>
    </Layout>
  );
}
