import * as React from "react";
import { render, waitFor, screen } from "utils/test-utils";
import "@testing-library/jest-dom/extend-expect";

import QuotesList from "./QuotesList";
import * as api from "services/api";

describe("test AnnouncementsList Component", () => {
  it("should render a list of quotes", async () => {
    const quotes = [
      {
        author: "Socrates",
        id: "27383733-1c5a-4344-84a0-1b2e905d1c02",
        text: "The unexamined life is not worth living.",
      },
      {
        author: "Heraclitus",
        id: "696913e0-2f90-4c5b-b9e1-2df7d1bbfe0a",
        text: "One cannot step twice in the same river.",
      },
    ];

    jest
      .spyOn(api, "getQuotes" as never)
      .mockImplementation(() => quotes as never);

    render(<QuotesList />);

    await waitFor(() => {
      expect(
        screen.getByText(/The unexamined life is not worth living./)
      ).toBeInTheDocument();
    });

    expect(
      screen.getByText(/One cannot step twice in the same river./)
    ).toBeInTheDocument();
  });
});
