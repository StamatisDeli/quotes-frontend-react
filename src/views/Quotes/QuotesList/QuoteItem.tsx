import React from "react";
import { NavLink, useLocation } from "react-router-dom";

import { QuoteType } from "types";

interface Props {
  quote: QuoteType;
  onScrollTo?: (el: HTMLAnchorElement) => void;
}

export default function QuoteItem({ quote, onScrollTo }: Props): JSX.Element {
  const { pathname } = useLocation();
  const id = pathname.split("/")[1];
  const isCurrent = quote.id === id;

  const handleScroll = (el: HTMLAnchorElement) => {
    el && onScrollTo && isCurrent && onScrollTo(el);
  };

  return (
    <NavLink
      ref={handleScroll}
      to={`/${quote.id}`}
      key={quote.id}
      className={`${
        isCurrent && "bg-gray-200"
      }  py-4 px-4 flex cursor-pointer items-center relative flex-shrink-0 flex-grow-0 border-b ${
        !isCurrent && "hover:bg-gray-100"
      }`}
    >
      <div className="text-left">
        <p className="text-gray-500 text-xl mb-2">"{quote.text}"</p>
        <p
          className={`${
            isCurrent ? "text-gray-600" : "text-gray-400"
          } font-semibold`}
        >
          {quote.author}
        </p>
      </div>
    </NavLink>
  );
}
