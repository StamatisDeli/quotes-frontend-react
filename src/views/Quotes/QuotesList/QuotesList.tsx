import React from "react";
import { useQuery } from "react-query";
import toast from "react-hot-toast";

import { QuoteType } from "types";
import Loader from "components/Loader";
import * as api from "services/api";
import QuoteItem from "./QuoteItem";

export default function QuotesList(): JSX.Element {
  const isFirstRender = React.useRef<boolean>(true);
  const selectedRef = React.useRef<HTMLAnchorElement>();
  const { data, isLoading, isSuccess, error } = useQuery(
    "get-quotes",
    api.getQuotes,
    {
      onError: () => {
        toast.error("Failed to get quotes data");
      },
    }
  );

  const handleScroll = (el: HTMLAnchorElement) => {
    selectedRef.current = el;

    el &&
      isFirstRender.current && //only run the once
      el.scrollIntoView({
        behavior: "smooth",
        inline: "start",
      });

    isFirstRender.current = false;
  };

  const quotes = data ?? [];
  const hasQuotes = quotes.length > 0;

  return (
    <aside
      data-testid="sidebar"
      className="flex flex-col h-full text-center flex-shrink-0 bg-white border-r border-gray-200 overflow-y-auto"
    >
      {isLoading && (
        <div className="flex flex-col p-5 h-full justify-center text-center text-red-400 flex-shrink-0 bg-white overflow-y-auto">
          <Loader />
        </div>
      )}

      {isSuccess &&
        hasQuotes &&
        quotes.map((quote: QuoteType) => (
          <QuoteItem key={quote.id} quote={quote} onScrollTo={handleScroll} />
        ))}

      {error && (
        <p className="flex flex-col p-10 h-full justify-center text-center text-red-400 flex-shrink-0 bg-white overflow-y-auto">
          Error loading quotes
        </p>
      )}

      {!hasQuotes && !isLoading && !error && (
        <p className="flex flex-col p-5 h-full justify-center text-center flex-shrink-0 bg-white overflow-y-auto">
          No quotes data
        </p>
      )}
    </aside>
  );
}
