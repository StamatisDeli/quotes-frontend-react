import React from "react";
import { useQuery } from "react-query";
import { useParams } from "react-router-dom";
import * as Yup from "yup";
import { ErrorMessage, Field, Form, Formik } from "formik";
import toast from "react-hot-toast";
import { useNavigate } from "react-router-dom";

import Layout from "components/Layout";
import Loader from "components/Loader";
import { FormType } from "types";
import * as api from "services/api";

const validationSchema = Yup.object().shape({
  author: Yup.string(),
  text: Yup.string().required("Please enter text"),
});

export default function EditQuote(): JSX.Element {
  const { id } = useParams<string>();
  const [isSaving, setIsSaving] = React.useState<boolean>(false);
  const navigate = useNavigate();

  const { data, isLoading, isSuccess, error } = useQuery(
    ["get-quote", id],
    () => api.getQuote(id as string),
    {
      onError: () => {
        toast.error("Failed to get quote data");
      },
      retry: false,
    }
  );

  const handleSubmit = async (values: FormType) => {
    try {
      setIsSaving(true);

      await api.putQuote(id as string, values);

      toast.success("Successfully stored changes");
    } catch (err) {
      toast.error("Failed to submit changes");
    } finally {
      setIsSaving(false);
    }
  };

  const handleDelete = async () => {
    try {
      setIsSaving(true);
      await api.deleteQuote(id as string);

      toast.success("Successfully deleted quote");
      navigate("/");
    } catch (err) {
      toast.error("Failed to delete quote");
    } finally {
      setIsSaving(false);
    }
  };

  const quote = data ?? {};

  return (
    <Layout>
      <article
        data-testid="edit-quote"
        className="flex flex-col mx-auto p-14 h-full bg-white overflow-y-auto max-w-2xl w-full justify-center"
      >
        <section className="mb-5">
          <h1 className="text-gray-800 text-3xl">Edit Quote*</h1>
          <p className="text-gray-500 text-xs">*Try not to mess it up</p>
        </section>

        {isLoading && <Loader />}

        {error && !isLoading && (
          <p className="flex flex-col p-5 h-full justify-center text-center text-red-400 flex-shrink-0 bg-white overflow-y-auto">
            Error loading quote
          </p>
        )}

        {isSuccess && (
          <Formik
            enableReinitialize
            initialValues={{
              author: quote.author ?? "",
              text: quote.text ?? "",
            }}
            validationSchema={validationSchema}
            onSubmit={handleSubmit}
          >
            {() => {
              return (
                <Form className="text-left flex flex-col text-gray-800">
                  <section className="mb-12">
                    <label
                      htmlFor="author"
                      className="font-semibold text-sm text-gray-550"
                    >
                      Author
                    </label>

                    <Field
                      id="author"
                      name="author"
                      type="text"
                      placeholder="Enter author"
                      data-testid="name-input"
                      className="mt-2 px-2 py-2 text-small w-full border border-gray-50 focus:border-blue-400 focus:outline-none focus:ring-4 focus:ring-blue-200"
                    />

                    <ErrorMessage
                      name="author"
                      component="div"
                      className="text-red-600 text-xs mt-1"
                    />
                  </section>

                  <section className="mb-5">
                    <label
                      htmlFor="text"
                      className="mb-1 font-semibold text-sm text-gray-550"
                    >
                      Text
                    </label>

                    <Field
                      id="text"
                      name="text"
                      type="text"
                      as="textarea"
                      placeholder="Enter quote text"
                      data-testid="text-input"
                      className="mt-2 px-2 py-2 text-small w-full border border-gray-50 focus:border-blue-400 focus:outline-none focus:ring-4 focus:ring-blue-200"
                    />

                    <ErrorMessage
                      name="text"
                      component="div"
                      className="text-red-600 text-xs mt-1"
                    />
                  </section>

                  <section className="flex w-full justify-between">
                    <button
                      data-testid="back-button"
                      className="border bg-gray-600 text-white rounded-md py-2 px-4 uppercase font-bold"
                      type="button"
                      disabled={isSaving}
                      onClick={() => navigate("/")}
                    >
                      Back
                    </button>

                    <button
                      data-testid="delete-button"
                      className="border bg-red-600 text-white rounded-md py-2 px-4 uppercase font-bold"
                      type="button"
                      disabled={isSaving}
                      onClick={handleDelete}
                    >
                      Delete
                    </button>

                    <button
                      data-testid="submit-button"
                      className={`${
                        isSaving ? "bg-blue-300" : "bg-blue-550"
                      } border text-white rounded-md py-2 px-4 uppercase font-bold`}
                      type="submit"
                      disabled={isSaving}
                    >
                      Save
                    </button>
                  </section>
                </Form>
              );
            }}
          </Formik>
        )}
      </article>
    </Layout>
  );
}
