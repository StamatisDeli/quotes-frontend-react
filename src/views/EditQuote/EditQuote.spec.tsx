import * as React from "react";
import { render, fireEvent, waitFor, screen, cleanup } from "utils/test-utils";
import "@testing-library/jest-dom/extend-expect";

import EditQuote from "./EditQuote";
import * as api from "services/api";

describe("<EditQuote />", () => {
  jest.retryTimes(2);

  afterEach(() => {
    cleanup();
    jest.clearAllMocks();
  });

  it("should render the component", () => {
    render(<EditQuote />);

    expect(screen.getByTestId("edit-quote")).toBeInTheDocument();
  });

  it("should render a quote", async () => {
    const quote = {
      author: "Bishop George Berkeley",
      id: "836ecb44-e6d5-47a2-a254-28d939a4b530",
      text: "To be is to be perceived.",
    };

    jest
      .spyOn(api, "getQuote" as never)
      .mockImplementation(() => quote as never);

    render(<EditQuote />);

    await waitFor(() => {
      expect(screen.getByText(/To be is to be perceived./)).toBeInTheDocument();
    });
  });

  it("should not submit the form, if quote text is not submitted", async () => {
    const quote = {
      author: "Bishop George Berkeley",
      id: "836ecb44-e6d5-47a2-a254-28d939a4b530",
      text: "To be is to be perceived.",
    };

    jest
      .spyOn(api, "getQuote" as never)
      .mockImplementation(() => quote as never);

    render(<EditQuote />);

    await waitFor(() => screen.findByText(/To be is to be perceived./));

    const input = screen.getByTestId("text-input");
    const submitBtn = screen.getByTestId("submit-button");

    fireEvent.change(input, {
      target: { value: "" },
    });
    fireEvent.click(submitBtn);

    await waitFor(() => {
      expect(screen.getByText("Please enter text")).toBeInTheDocument();
    });
  });

  it("should submit the form, if quote text is submitted", async () => {
    const quote = {
      author: "Bishop George Berkeley",
      id: "836ecb44-e6d5-47a2-a254-28d939a4b530",
      text: "To be is to be perceived.",
    };
    jest
      .spyOn(api, "getQuote" as never)
      .mockImplementationOnce(() => quote as never);

    const mockPost = jest.spyOn(api, "putQuote" as never);

    render(<EditQuote />);

    await waitFor(() => screen.findByText(/To be is to be perceived./));

    fireEvent.click(screen.getByTestId("submit-button"));

    await waitFor(() => {
      // TODO: Why do I have to pass undefined here?
      expect(mockPost).toBeCalledWith(undefined, {
        author: "Bishop George Berkeley",
        text: "To be is to be perceived.",
      });
    });
  });
});
