import * as React from "react";
import { render, fireEvent, waitFor, screen } from "utils/test-utils";
import "@testing-library/jest-dom/extend-expect";

import NewQuote from "./NewQuote";
import * as api from "services/api";

describe("<NewQuote />", () => {
  it("should render the component", () => {
    render(<NewQuote />);

    expect(screen.getByTestId("new-quote")).toBeInTheDocument();
  });

  it("should not submit the form, if quote text is not submitted", async () => {
    render(<NewQuote />);

    fireEvent.click(screen.getByTestId("submit-button"));

    await waitFor(() => {
      expect(screen.getByText("Please enter text")).toBeInTheDocument();
    });
  });

  it("should submit the form, if quote text is submitted", async () => {
    const mockPost = jest.spyOn(api, "postQuote" as never);

    render(<NewQuote />);

    fireEvent.change(screen.getByTestId("text-input"), {
      target: { value: "the quote text" },
    });

    fireEvent.click(screen.getByTestId("submit-button"));

    await waitFor(() => {
      expect(mockPost).toBeCalledWith({
        author: "",
        text: "the quote text",
      });
    });
  });
});
