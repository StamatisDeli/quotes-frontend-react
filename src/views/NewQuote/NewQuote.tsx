import React from "react";
import * as Yup from "yup";
import { ErrorMessage, Field, Form, Formik } from "formik";
import toast from "react-hot-toast";
import { useNavigate } from "react-router-dom";
import Layout from "components/Layout";

import { FormType } from "types";
import * as api from "services/api";

const validationSchema = Yup.object().shape({
  author: Yup.string(),
  text: Yup.string().required("Please enter text"),
});

export default function NewQuote(): JSX.Element {
  const [isSaving, setIsSaving] = React.useState<boolean>(false);
  const navigate = useNavigate();

  const handleSubmit = async (values: FormType) => {
    try {
      setIsSaving(true);
      await api.postQuote(values);

      toast.success("Successfully stored changes");
      navigate("/");
    } catch (err) {
      toast.error("Failed to submit changes");
    } finally {
      setIsSaving(false);
    }
  };

  return (
    <Layout>
      <article
        data-testid="new-quote"
        className="flex flex-col mx-auto p-5 h-full bg-white overflow-y-auto max-w-2xl w-full"
      >
        <Formik
          enableReinitialize
          initialValues={{
            author: "",
            text: "",
          }}
          validationSchema={validationSchema}
          onSubmit={handleSubmit}
        >
          {() => {
            return (
              <Form
                data-testid="new-quote-form"
                className="text-left flex flex-col text-gray-800"
              >
                <section className="mb-5">
                  <h1 className="text-gray-800 text-3xl">Create New Quote*</h1>
                  <p className="text-gray-500 text-xs">*Do your worst</p>
                </section>

                <section className="mb-5">
                  <label
                    htmlFor="author"
                    className="font-semibold text-sm text-gray-550"
                  >
                    Author
                  </label>

                  <Field
                    id="author"
                    name="author"
                    type="text"
                    placeholder="Enter author"
                    data-testid="name-input"
                    className="mt-2 px-2 py-2 text-small w-full border border-gray-50 focus:border-blue-400 focus:outline-none focus:ring-4 focus:ring-blue-200"
                  />

                  <ErrorMessage
                    name="author"
                    component="div"
                    className="text-red-600 text-xs mt-1"
                  />
                </section>

                <section className="mb-5">
                  <label
                    htmlFor="text"
                    className="mb-1 font-semibold text-sm text-gray-550"
                  >
                    Text
                  </label>

                  <Field
                    id="text"
                    name="text"
                    type="text"
                    as="textarea"
                    placeholder="Enter quote text"
                    data-testid="text-input"
                    className="mt-2 px-2 py-2 text-small w-full border border-gray-50 focus:border-blue-400 focus:outline-none focus:ring-4 focus:ring-blue-200"
                  />

                  <ErrorMessage
                    name="text"
                    component="div"
                    className="text-red-600 text-xs mt-1"
                  />
                </section>

                <section className="flex w-full justify-end">
                  <button
                    data-testid="cancel-button"
                    className="bg-gray-500 border text-white rounded-md py-2 px-4 uppercase ml-3 font-bold"
                    type="button"
                    disabled={isSaving}
                    onClick={() => navigate("/")}
                  >
                    Cancel
                  </button>

                  <button
                    data-testid="submit-button"
                    className={`${
                      isSaving ? "bg-blue-300" : "bg-blue-550"
                    } border text-white rounded-md py-2 px-4 uppercase ml-3 font-bold`}
                    type="submit"
                    disabled={isSaving}
                  >
                    Save
                  </button>
                </section>
              </Form>
            );
          }}
        </Formik>
      </article>
    </Layout>
  );
}
