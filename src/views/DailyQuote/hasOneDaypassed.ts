// https://stackoverflow.com/questions/11741979/run-code-once-a-day
export function hasOneDayPassed() {
  // get today's date. eg: "7/37/2007"
  const date = new Date().toLocaleDateString();

  // if there's a date in localstorage and it's equal to the above:
  // inferring a day has yet to pass since both dates are equal.
  if (localStorage.quotes_app_date === date) {
    return false;
  }
  // this portion of logic occurs when a day has passed
  localStorage.quotes_app_date = date;

  return true;
}
