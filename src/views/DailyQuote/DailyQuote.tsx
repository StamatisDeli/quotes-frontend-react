import React from "react";
import toast from "react-hot-toast";
import { useNavigate } from "react-router-dom";
import { isEmpty } from "lodash";

import Layout from "components/Layout";
import Loader from "components/Loader";
import { QuoteType } from "types";
import * as api from "services/api";
import { hasOneDayPassed } from "./hasOneDaypassed";

export default function DailyQuote(): JSX.Element {
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = React.useState<boolean>(false);
  const [randomQuote, setRandomQuote] = React.useState<QuoteType>();

  React.useEffect(() => {
    const getDailyQuote = async () => {
      setIsLoading(true);

      try {
        const randomQuote = await api.getRandomQuote();

        setRandomQuote(randomQuote[0]);
        localStorage.setItem("daily_quote", JSON.stringify(randomQuote[0]));

        setIsLoading(false);
      } catch (err) {
        console.log(err);
        toast.error("Failed to get daily quote");
        setIsLoading(false);
      }
    };

    const getStorage = async () => await localStorage.getItem("daily_quote");

    if (hasOneDayPassed()) {
      getDailyQuote();
    }
    if (!hasOneDayPassed()) {
      getStorage().then((quote) => {
        setRandomQuote(JSON.parse(quote as string));
      });
    }
  }, []);

  const hasQuote = !isEmpty(randomQuote);

  return (
    <Layout>
      <article
        data-testid="daily-quote"
        className="flex flex-col mx-auto p-14 h-full bg-white overflow-y-auto max-w-2xl w-full justify-center"
      >
        {isLoading && <Loader />}

        {hasQuote && (
          <>
            <h2 className="text-5xl mb-4">"{randomQuote?.text}"</h2>
            <h4 className="text-lg mb-10">{randomQuote?.author}</h4>
            <p className="mb-20 text-center text-gray-400">
              come back tomorrow for a fresh quote
            </p>
          </>
        )}

        <div className="w-full text-center mx-auto">
          <button
            className="rounded-md border py-2 px-4"
            onClick={() => navigate("/")}
          >
            Back
          </button>
        </div>
      </article>
    </Layout>
  );
}
