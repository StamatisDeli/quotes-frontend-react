import * as React from "react";
import { render, fireEvent, waitFor, screen, cleanup } from "utils/test-utils";
import "@testing-library/jest-dom/extend-expect";

import DailyQuote from "./DailyQuote";
import * as api from "services/api";
import * as hasOneDayPassed from "./hasOneDaypassed";

describe("<EditQuote />", () => {
  afterEach(() => {
    cleanup();
  });

  it("should render the component", () => {
    render(<DailyQuote />);

    expect(screen.getByTestId("daily-quote")).toBeInTheDocument();
  });

  it("should render a quote", async () => {
    const quote = [
      {
        author: "Bishop George Berkeley",
        id: "836ecb44-e6d5-47a2-a254-28d939a4b530",
        text: "To be is to be perceived.",
      },
    ];

    jest
      .spyOn(hasOneDayPassed, "hasOneDayPassed" as never)
      .mockImplementation(() => true as never);

    jest
      .spyOn(api, "getRandomQuote" as never)
      .mockImplementation(() => quote as never);

    render(<DailyQuote />);

    await waitFor(() => {
      expect(screen.getByText(/To be is to be perceived./)).toBeInTheDocument();
    });
  });
});
