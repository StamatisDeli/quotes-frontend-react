import React from "react";
import "./App.css";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import { Routes, Route, Navigate } from "react-router-dom";

import Quotes from "views/Quotes";
import EditQuote from "views/EditQuote";
import NewQuote from "views/NewQuote";
import DailyQuote from "views/DailyQuote";

function App() {
  return (
    <Routes>
      <Route path="/" element={<Quotes />} />
      <Route path="/:id" element={<EditQuote />} />
      <Route path="/new-quote" element={<NewQuote />} />
      <Route path="/quote-daily" element={<DailyQuote />} />
      <Route path="*" element={<Navigate to="/" />} />
    </Routes>
  );
}

export default App;
