import request from "./request";
import { FormType } from "../types";

export const getQuotes = async () => {
  const { data } = await request.get(`/`);

  return data;
};

export const getQuote = async (id: string) => {
  const { data } = await request.get(`/${id}`);

  return data;
};

export const getRandomQuote = async () => {
  const { data } = await request.get(`/random`);

  return data;
};

export const putQuote = async (id: string, formData: FormType) => {
  const { data } = await request.put(`/${id}`, formData);

  return data;
};

export const postQuote = async (formData: FormType) => {
  const { data } = await request.post(`/`, formData);

  return data;
};

export const deleteQuote = async (id: string) => {
  const { data } = await request.delete(`/${id}`);

  return data;
};
