/// <reference types="react-scripts" />
declare module "tailwindcss/resolveConfig" {
  const resolveConfig: (config: unknown) => {
    theme: Record<string, any>;
  };
  export default resolveConfig;
}
