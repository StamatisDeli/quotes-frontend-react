import React from "react";
import { render } from "@testing-library/react";
import { QueryClientProvider, QueryClient } from "react-query";
import { ThemeProvider } from "styled-components";
import { MemoryRouter } from "react-router-dom";
import resolveConfig from "tailwindcss/resolveConfig";

import tailwindConfig from "../../postcss.config.js";

const queryClient = new QueryClient();

const AllTheProviders = ({ children }: any) => {
  return (
    // <ThemeProvider theme={resolveConfig(tailwindConfig).theme}>
    <MemoryRouter>
      <QueryClientProvider client={queryClient}>{children}</QueryClientProvider>
    </MemoryRouter>
    // </ThemeProvider>
  );
};

const customRender = (ui: any): any => render(ui, { wrapper: AllTheProviders });

// re-export everything
export * from "@testing-library/react";

// override render method
export { customRender as render };
