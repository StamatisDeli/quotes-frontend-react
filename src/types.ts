export interface QuoteType {
  id: string;
  author: string;
  text: string;
}

export interface FormType {
  author: string;
  text: string;
}
